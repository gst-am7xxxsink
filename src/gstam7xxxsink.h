/* GStreamer
 * Copyright (C) 2013  Antonio Ospite <ospite@studenti.unina.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_AM7XXXSINK_H__
#define __GST_AM7XXXSINK_H__

#include <gst/gst.h>
#include <gst/video/gstvideosink.h>
#include <gst/video/video.h>

#include <am7xxx.h>

G_BEGIN_DECLS

#define GST_TYPE_AM7XXXSINK \
  (gst_am7xxxsink_get_type())
#define GST_AM7XXXSINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_AM7XXXSINK,GstAM7XXXSink))
#define GST_AM7XXXSINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_AM7XXXSINK,GstAM7XXXSinkClass))
#define GST_IS_AM7XXXSINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_AM7XXXSINK))
#define GST_IS_AM7XXXSINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_AM7XXXSINK))

typedef struct _GstAM7XXXSink GstAM7XXXSink;
typedef struct _GstAM7XXXSinkClass GstAM7XXXSinkClass;

struct _GstAM7XXXSink {
  GstVideoSink videosink;

  unsigned int device_index;

  unsigned int width, height;

  am7xxx_context *ctx;
  am7xxx_device *dev;

  am7xxx_image_format format;
  am7xxx_device_info device_info;
};

struct _GstAM7XXXSinkClass {
  GstVideoSinkClass videosink_class;
};

GType gst_am7xxxsink_get_type(void);

G_END_DECLS

#endif /* __GST_AM7XXXSINK_H__ */
