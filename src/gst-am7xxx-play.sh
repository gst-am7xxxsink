#!/bin/sh

set -e

SRC="$1"
URI="${2:-http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_640x360.m4v}"

usage() {
  echo "usage: $(basename $0) [test|ximage|uri [URI]|v4l2|help]"
}

case "$SRC" in
  test)
    INPUT="videotestsrc ! video/x-raw,width=1920,height=1080"
    ;;

  ximage)
    # without use-damage=0 ximagesrc is very slow, see:
    # https://bugzilla.gnome.org/show_bug.cgi?id=693037
    INPUT="ximagesrc use-damage=0 show-pointer=1 ! video/x-raw,framerate=30/1"
    ;;

  uri)
    DECODER="dec"
    INPUT="uridecodebin uri=\"$URI\" name=$DECODER ${DECODER}."
    ;;

  v4l2)
    INPUT="v4l2src always-copy=FALSE ! video/x-raw,width=640,height=480"
    ;;

  help|-h|--help)
    usage
    exit 0
    ;;

  *)
    usage 1>&2
    exit 1
    ;;
esac

JPEG_FORMAT=1

if [ $JPEG_FORMAT -eq 1 ];
then
  # am7xxx devices doesn't like jpegs with subsampling other than 420
  ENCODER="! video/x-raw,format=I420 ! jpegenc"
fi

VIDEOCONVERT="videorate ! videoscale add-borders=1 ! videoconvert $ENCODER"

VIDEOSINK="am7xxxsink"
#VIDEOSINK="autovideosink"
#VIDEOSINK="fpsdisplaysink text-overlay=false video-sink=$VIDEOSINK"

VIDEO_OUT="$VIDEOCONVERT ! $VIDEOSINK"

if [ "x$DECODER" != "x" ];
then
  AUDIO_OUT="${DECODER}. ! audioconvert ! autoaudiosink"
fi

LANG=C \
GST_DEBUG=am7xxxsink:6 \
GST_PLUGIN_PATH=$(pwd) \
gst-launch-1.0 $INPUT ! $VIDEO_OUT $AUDIO_OUT
